package com.interfaces.intelikids;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;

public class VariosActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.varios_menu);

	}
	
	public void facilBtn(View view) {
	    Intent intent = new Intent(this, facilVarios.class);
	    startActivity(intent);
	}
	
	public void medioBtn(View view) {
	    Intent intent = new Intent(this, medioVarios.class);
	    startActivity(intent);
	}
	
	public void dificilBtn(View view) {
	    Intent intent = new Intent(this, dificilVarios.class);
	    startActivity(intent);
	}

}
