package com.interfaces.intelikids;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;

public class SumaActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.suma_menu);

	}
	
	public void facilBtn(View view) {
	    Intent intent = new Intent(this, facilSuma.class);
	    startActivity(intent);
	}
	
	public void medioBtn(View view) {
	    Intent intent = new Intent(this, medioSuma.class);
	    startActivity(intent);
	}
	
	public void dificilBtn(View view) {
	    Intent intent = new Intent(this, dificilSuma.class);
	    startActivity(intent);
	}
	
	
}
