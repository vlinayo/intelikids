package com.interfaces.intelikids;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;

public class ProgressActivity extends ActionBarActivity {

	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.progreso_menu);
	}
	
	public void facilBtn(View view) {
	    Intent intent = new Intent(this, facilProgress.class);
	    startActivity(intent);
	}
	
	public void medioBtn(View view) {
	    Intent intent = new Intent(this, medioProgress.class);
	    startActivity(intent);
	}
	
	public void dificilBtn(View view) {
	    Intent intent = new Intent(this, dificilProgress.class);
	    startActivity(intent);
	}

}
