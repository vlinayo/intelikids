package com.interfaces.intelikids;

import java.util.List;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;

public class MainActivity extends ActionBarActivity {

    MediaPlayer mp;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
        mp = MediaPlayer.create(MainActivity.this, R.raw.mika);
        mp.setLooping(true);
        mp.start();
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}

	 @Override
	    protected void onPause(){
	    	
	    	if (this.isFinishing()){
	    		mp.stop();
	    	}
	    	
	    	Context context = getApplicationContext();
	    	ActivityManager am= (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
	    	List<RunningTaskInfo> taskInfo = am.getRunningTasks(1);
	    	if(!taskInfo.isEmpty()){
	    		ComponentName topActivity = taskInfo.get(0).topActivity;
	    		if(!topActivity.getPackageName().equals(context.getPackageName())){
	    			mp.stop();
	    		}else{
	    			
	    		}
	    	}
	    	super.onPause();
	    } 
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}
	
	public void LogBtn(View view) {
	    Intent intent = new Intent(this, PrincipalScreen.class);
	    startActivity(intent);
	}
	
}
